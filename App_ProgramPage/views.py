from django.shortcuts import render
from .models import ProgramsModel
from App_NewsPage.models import News
# Create your views here.

def index(request):
    table = ProgramsModel.objects.all()
    response = {'table' : table}
    return render(request, 'App_ProgramPage_index.html', response)

def retrieve(request, id):
    news = News.objects.get(id = id)
    programs = ProgramsModel.objects.filter(foreign_news=news)
    response = {'table' : programs}
    return render(request, 'App_ProgramPage_index.html', response)