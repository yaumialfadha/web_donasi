from django.urls import path
from .views import make_donation

urlpatterns = [
    path('<int:id>/', make_donation, name='make_donation'),
]