from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator

class Donate(models.Model):
	donator = models.CharField(max_length=50, default="Anonymous")
	amount = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1)])
	foreign_program = models.ForeignKey('App_ProgramPage.ProgramsModel', on_delete=models.CASCADE, default=1)
	published_date = models.DateTimeField(null=True, auto_now_add=True)

	def submit(self):
		self.published_date = timezone.now()
		self.save()

	def __str__(self):
		return self.donator_name