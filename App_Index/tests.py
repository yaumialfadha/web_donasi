from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import index
# Create your tests here.

class indexTest(TestCase):

    def test_index_is_exist(self):
        response = Client().get(reverse('index'))
        self.assertEqual(response.status_code,200)

    def test_index_using_template(self):
        response = Client().get(reverse('index'))
        self.assertTemplateUsed(response, 'App_Index_index.html')

    def test_index_using_correct_views_func(self):
        found = resolve(reverse('index'))
        self.assertEqual(found.func, index)