from django.shortcuts import render, get_object_or_404
from .forms import TestiForm
from .models import Testimoni
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
# Create your views here.

def index(request):
    form = TestiForm()
    testimonies = Testimoni.objects.all()
    return render(request, 'App_Index_index.html', {"form": form, 'testimonies': testimonies})

@login_required
def add_testimoni(request):
    if request.method == "POST":
        form = TestiForm(request.POST)
        if form.is_valid():
            activity = form.save(commit=False)
            activity.author = request.user
            activity.save()
            return JsonResponse({"result" : "Success! You added a comment", "name":activity.testi_name, "content":activity.testi_message}, safe=False)
        else:
            return JsonResponse({"result" : "Error! something bad happened!"}, safe=False)

