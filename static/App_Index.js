$("#submit_button").click((e) => {
        e.preventDefault();
        $.ajax({
            url : "/add_testimoni", 
            type : "POST",
            headers: {
                'X-CSRF-Token': $("input[name=csrfmiddlewaretoken]").val()
            },
            data : $("form").serialize(), 
            dataType: 'json',
            success : function(resp) {
                alert(resp.result);
                $("#comment-container").append(
                    `
                    <div class="col-lg-2">
                    <p>`+resp.name+`</p>
                </div>
                <div class="col-lg-10">
                    <p>`+resp.content+`</p>
                </div>
                    `
                );
            },
            error : function(resp){
                alert (resp.result);
            }
        });
    });
