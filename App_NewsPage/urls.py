from django.urls import path
from .views import newsPage

urlpatterns = [
    path('', newsPage, name='newsPage')
]