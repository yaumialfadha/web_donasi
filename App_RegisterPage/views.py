from django.contrib.auth import login, authenticate
from django.contrib.auth import logout as logout_user
from django.shortcuts import render, redirect
from .models import SignUpForm 
from .forms import SignUpForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
              

            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/register')
    else:
        form = SignUpForm()
    return render(request, 'App_RegisterPage_index.html', {'form': form})

def logout(request):
    logout_user(request)
    return redirect('/')

def login(request):
    return redirect('/register')
